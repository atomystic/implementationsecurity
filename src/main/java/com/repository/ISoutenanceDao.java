package com.repository;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entity.Soutenance;

@Repository
public interface ISoutenanceDao extends JpaRepository<Soutenance, Integer> {

	@Query("SELECT s FROM Soutenance AS s WHERE s.sujet.encadrant.id=:pId OR s.sujet.president.id=:pId OR s.sujet.rapporteur.id=:pId")
	public List<Soutenance> findSoutenanceByEnseignant(@Param("pId") int id);
	
	
}
