package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Enseignant;

@Repository
public interface IEnseignantDao extends JpaRepository<Enseignant, Integer>{

	
	
}
