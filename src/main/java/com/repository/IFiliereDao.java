package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Filiere;

@Repository
public interface IFiliereDao extends JpaRepository<Filiere, Integer>{

}
