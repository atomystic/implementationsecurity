package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Session;

@Repository
public interface ISessionDao extends JpaRepository<Session, Integer >{
	
	
	

}
