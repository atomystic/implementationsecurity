package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Rapport;

@Repository
public interface IRapportDao extends JpaRepository<Rapport, Integer>{

}
