package com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Sujet;

@Repository
public interface ISujetDao extends JpaRepository<Sujet, Integer>{
	
	public List<Sujet> findByValide(short valide);
	

}
