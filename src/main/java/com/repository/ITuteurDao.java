package com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entity.Tuteur;

@Repository
public interface ITuteurDao extends JpaRepository<Tuteur, Integer>{
	
	@Query("SELECT t FROM Tuteur AS t WHERE t.organisme.id=:pId")
	public List<Tuteur> findTuteurByOrganisme(@Param("pId")int id);

}
