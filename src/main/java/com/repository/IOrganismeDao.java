package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Organisme;

@Repository
public interface IOrganismeDao extends JpaRepository<Organisme, Integer>{

}
