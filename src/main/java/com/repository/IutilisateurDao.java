package com.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.entity.Utilisateur;

@Repository
public interface IutilisateurDao extends JpaRepository<Utilisateur,Integer>{
	
	Optional<Utilisateur> findByLogin (String username);

	Boolean existsByLogin(String username);

}
