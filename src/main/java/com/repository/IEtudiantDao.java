package com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entity.Etudiant;

@Repository
public interface IEtudiantDao extends JpaRepository<Etudiant, Integer>{
	
	@Query("SELECT e FROM Etudiant AS e WHERE e.sujet.id=:pId")
	public List<Etudiant> findEtudiantBySujet(@Param("pId") int id);
}
