package com.controleurs;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Etudiant;
import com.entity.Filiere;
import com.service.IEtudiantService;
import com.service.IFiliereService;

@RestController
@RequestMapping("/etudiant")
@CrossOrigin(origins = "*")
public class EtudiantRest {

	//transfo assoc uml en java
	@Autowired
	private IEtudiantService etuService;
	
	@Autowired
	private IFiliereService fService;

			
	@PostMapping(value = "/add/{fId}", produces = "application/json")
	public Etudiant addEtudiant(@RequestBody Etudiant etu, @PathVariable("fId") int fId) {
		Filiere fIn = fService.getFiliereById(fId);
		etu.setFiliere(fIn);
		return etuService.addEtudiant(etu);
	}

	@DeleteMapping(value = "/del/{id}")
	public void delEtudiantById(@PathVariable("id") int id) {
		// appel methode service
		etuService.delEtudiantById(id);;
		
	}

	@GetMapping(value = "/getAll", produces = "application/json")
	public List<Etudiant> getAllEtudiant() {
		// appel methode service
		return etuService.getAllEtudiant();
	}

	@GetMapping(value = "/getById/{id}", produces = "application/json")
	public Etudiant getEtudiantById(@PathVariable("id") int id) {
		// appel methode service
		return etuService.getEtudiantById(id);
	}

	@PutMapping(value = "/update/{fId}", produces = "application/json")
	public Etudiant updateEtudiant(@RequestBody Etudiant etu, @PathVariable("fId") int fId) {
		Filiere fIn = fService.getFiliereById(fId);
		etu.setFiliere(fIn);
		return etuService.updateEtudiant(etu);
	}
	
	@GetMapping(value = "/findBySujet/{id}", produces = "application/json")
	public List<Etudiant> findEtudiantBySujet(@PathVariable("id")int id) {
		//appel methode service
		return etuService.findEtudiantBySujet(id);
	}
	
}
