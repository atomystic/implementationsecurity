package com.controleurs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Organisme;
import com.entity.Tuteur;
import com.service.IOrganismeService;
import com.service.ITuteurService;

@RestController
@RequestMapping("/tuteur")
@CrossOrigin(origins = "*")
public class TuteurRest {
	
	//transfo assoc uml en java
	@Autowired
	private ITuteurService tutService;
	
	@Autowired
	private IOrganismeService oService;
	
	
	// ======= AJOUTER TUTEUR =========
	@PostMapping(value = "/add/{o_id}", produces = "application/json")
	public Tuteur addTuteur(@RequestBody Tuteur tut, @PathVariable("o_id") int id) {
		Organisme organisme = oService.getOrganismeById(id);
		tut.setOrganisme(organisme);
		return tutService.addTuteur(tut);
	}
	
	// ======= SUPPRIMER TUTEUR =======
	@DeleteMapping(value = "/del/{id}")
	public void delTuteurById(@PathVariable("id") int id) {
		
		tutService.delTuteurById(id);
	}
	
	// ====== LISTE DES TUTEURS =====
	@GetMapping(value = "/getAll", produces = "application/json")
	public List<Tuteur> getAllTuteur(){
		
		return tutService.getAllTuteur();
	}
	
	// ====== RECHERCHER UN TUTEUR ======
	@GetMapping(value = "/getById/{id}", produces = "application/json")
	public Tuteur getTuteurById(@PathVariable("id") int id) {
		
		return tutService.getTuteurById(id);
	}
	
	// ======= MODIFIER UN TUTEUR ======
	@PutMapping(value = "/update/{o_id}", produces = "application/json")
	public Tuteur updateTuteur(@RequestBody Tuteur tut, @PathVariable("o_id") int id) {
		Organisme organisme = oService.getOrganismeById(id);
		tut.setOrganisme(organisme);
		return tutService.updateTuteur(tut);
	}
	
	// ====== RECHERCHER UN TUTEUR PAR ORGANISME =====
	@GetMapping(value = "/findByOrganisme/{id}", produces = "application/json")
	public List<Tuteur> findTuteurByOrganisme(@PathVariable("id")int id) {
		return tutService.findTuteurByOrganisme(id);
	}

}
