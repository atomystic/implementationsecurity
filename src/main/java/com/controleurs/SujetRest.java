package com.controleurs;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Enseignant;
import com.entity.Etudiant;
import com.entity.Filiere;
import com.entity.Soutenance;
import com.entity.Sujet;
import com.entity.Tuteur;
import com.repository.IEtudiantDao;
import com.service.IEnseignantService;
import com.service.IEtudiantService;
import com.service.IFiliereService;
import com.service.ISoutenanceService;
import com.service.ISujetService;
import com.service.ITuteurService;
import com.service.imp.SujetServiceImpl;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/sujet")
public class SujetRest {

	@Autowired
	private ISujetService sService;

	@Autowired
	private IEnseignantService eService;

	@Autowired
	private ISoutenanceService soutenanceService;

	@Autowired
	private ITuteurService tuteurService;

	@Autowired
	private IEtudiantService etuService;

	@Autowired
	private IFiliereService fService;

	// CRUD
	@PostMapping(value = "/add/{f_id}/{t_id}/{e1_id}/{e2_id}", produces = "application/json")
	public Sujet addSujet(@RequestBody Sujet sujet, @PathVariable("f_id") int fId, @PathVariable("t_id") int tId, @PathVariable("e1_id") int e1Id, @PathVariable("e2_id") int e2Id) {


		// filiere
		Filiere filiere = fService.getFiliereById(fId);
		sujet.setFiliere(filiere);

		// tuteur
		Tuteur tuteur = tuteurService.getTuteurById(tId);
		sujet.setTuteur(tuteur);

		// binome d'etudiants
		Etudiant etudiant1 = etuService.getEtudiantById(e1Id);
		Etudiant etudiant2 = etuService.getEtudiantById(e2Id);

		if (etudiant1.getId() != etudiant2.getId()) {
			
//			List<Etudiant> binome = new ArrayList<Etudiant>();
//			binome.add(etudiant1);
//			binome.add(etudiant2);
//			sujet.setEtudiants(binome);
			
			etudiant1.setSujet(sujet);
			etudiant2.setSujet(sujet);
			
		} else {
//			List<Etudiant> solo = new ArrayList<Etudiant>();
//			solo.add(etudiant1);
//			sujet.setEtudiants(solo);
			etudiant1.setSujet(sujet);
		}

		return sService.addSujet(sujet);
	}

	@DeleteMapping(value = "/delById/{pId}")
	public void delSujetById(@PathVariable("pId") int id) {

		sService.delSujetById(id);
	}
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping(value = "/getAll", produces = "application/json")
	public String getAllSujet() {

		return "je suis la";
	}
//
//	@GetMapping(value = "/getAll", produces = "application/json")
//	public List<Sujet> getAllSujet() {
//
//		return sService.getAllSujet();
//	}
	@PutMapping(value = "/update/{p_id}/{r_id}/{e_id}/{f_id}/{t_id}/{s_id}/{e1_id}/{e2_id}", produces = "application/json")
	public Sujet updateSujet(@RequestBody Sujet sIn, @PathVariable("p_id") int pId, @PathVariable("r_id") int rId,
			@PathVariable("e_id") int eId, @PathVariable("f_id") int fId, @PathVariable("t_id") int tId,
			@PathVariable("s_id") int sId, @PathVariable("e1_id") int e1Id, @PathVariable("e2_id") int e2Id) {
		System.out.println("-------------------------------"+pId);
		
		
		
		
		if (pId !=0 || rId != 0 || eId != 0) {
			// encadrant
			if(eId != 0) {
				Enseignant encadrant = eService.getEnseignantById(eId);
				sIn.setEncadrant(encadrant);
				
				// president
				if(pId != 0 && pId != eId) {
					 Enseignant president = eService.getEnseignantById(pId);
					sIn.setPresident(president);
					
					// rapporteur
					
					if(rId != 0 && rId != eId && rId != pId) {
						 Enseignant rapporteur = eService.getEnseignantById(rId);
						sIn.setRapporteur(rapporteur);
					}//fin du if rapporteur different de zero encadrant et president
				}else {// fin du if president different de zero et encadrant
					sIn.setPresident(null);
					sIn.setRapporteur(null);
				}
			}// fin du if encadrant different de zero
		}//fin du if l'un d'entre eux est différent de zéro

		// filiere
		Filiere filiere = fService.getFiliereById(fId);
		sIn.setFiliere(filiere);

		// tuteur
		Tuteur tuteur = tuteurService.getTuteurById(tId);
		sIn.setTuteur(tuteur);

		// soutenance
		if(sId != 0) {
		Soutenance soutenance = soutenanceService.getSoutenanceById(sId);
		sIn.setSoutenance(soutenance);
		} else {
			sIn.setSoutenance(null);
		}

		// binome d'etudiants
		Etudiant etudiant1 = etuService.getEtudiantById(e1Id);
		Etudiant etudiant2 = etuService.getEtudiantById(e2Id);

		if (etudiant1.getId() != etudiant2.getId()) {
			List<Etudiant> binome = new ArrayList<Etudiant>();
			binome.add(etudiant1);
			binome.add(etudiant2);
			sIn.setEtudiants(binome);
		} else {
			List<Etudiant> solo = new ArrayList<Etudiant>();
			solo.add(etudiant1);
			sIn.setEtudiants(solo);
		}

		return sService.updateSujet(sIn);
	}

	@GetMapping(value = "/getById/{pId}", produces = "application/json")
	public Sujet getSujetById(@PathVariable("pId") int id) {

		return sService.getSujetById(id);

	}

	// Méthodes métier

	@GetMapping(value = "/getByValide/{pValide}", produces = "application/json")
	public List<Sujet> getSujetByValide(@PathVariable("pValide") int valide) {
		return sService.getSujetByValide((short) valide);
	}

	@GetMapping(value = "/getByNonValide", produces = "application/json")
	public List<Sujet> getSujetNonValide() {
		return sService.getSujetNonValide();
	}

	@GetMapping(value = "/valider/{pIdS}/{pIdE}", produces = "application/json")
	public Sujet validerSujet(@PathVariable("pIdS") int idSujet, @PathVariable("pIdE") int idValidateur) {
		Enseignant valideur = eService.getEnseignantById(idValidateur);
		Sujet sIn = sService.getSujetById(idSujet);
		return sService.validerSujet(sIn, valideur);
	}

}
