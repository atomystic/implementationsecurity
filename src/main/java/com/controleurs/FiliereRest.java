package com.controleurs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Filiere;
import com.service.IFiliereService;

@RestController
@RequestMapping("/filiere")
@CrossOrigin(origins = "*")
public class FiliereRest {
	
	@Autowired
	private IFiliereService fService;
	
	// Ajouter filiere
	@PostMapping(value = "/add", produces = "application/json")
	public Filiere addFiliere(@RequestBody Filiere filiere) {
		return fService.addFiliere(filiere);
	}
	
	// Supprimer filiere
	@DeleteMapping(value ="/del/{pId}")
	public void delFiliereById(@PathVariable("pId") int id) {
		fService.delFiliereById(id);
	}
	
	// Get All
	@GetMapping(value = "/getAll", produces = "application/json")
	public List<Filiere> getAllFiliere(){
		return fService.getAllFiliere();
	}
	
	// Get By Id
	@GetMapping(value = "/getById/{pId}", produces = "application/json")
	public Filiere getFiliereById(@PathVariable("pId") int id) {
		return fService.getFiliereById(id);
	}
	
	// Modifier
	@PutMapping(value = "/update", produces = "application/json")
	public Filiere updateFiliere(@RequestBody Filiere filiere) {
		return fService.updateFiliere(filiere);
	}

}
