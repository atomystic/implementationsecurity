package com.controleurs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Organisme;
import com.service.IOrganismeService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/organisme")
public class OrganismeRest {
	
	//transfo UML java
	@Autowired
	private IOrganismeService oService;
	
	//add organisme
	@PostMapping(value = "/add", produces = "application/json")
	public Organisme addOrganisme(@RequestBody Organisme o) {
		return oService.addOrganisme(o);
	}

	//delete organisme
	@DeleteMapping(value = "/del/{pId}")
	public  void delOrganisme(@PathVariable("pId")int id) {
		oService.delOrganismeById(id);
	}
	
	//get all Organisme
	@GetMapping(value = "/getAll", produces = "application/json" )
	public List<Organisme> getListe() {
		return oService.getAllOrganisme();
	}
	
	//get by ID
	@GetMapping(value = "/getById/{pId}", produces = "application/json")
	public Organisme getOrganismeById(@PathVariable("pId")int id) {
		return oService.getOrganismeById(id);
	}
	
	//update organisme 
	@PutMapping(value = "/update", produces="application/json")
	public Organisme updateOrganisme(@RequestBody Organisme o) {
		return oService.updateOrganisme(o);
	}
}
