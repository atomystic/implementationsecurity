package com.controleurs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Filiere;
import com.entity.Session;
import com.service.IFiliereService;
import com.service.ISessionService;

@RestController
@RequestMapping("/session")
@CrossOrigin(origins = "*")
public class SessionRest {
	
	//Lien UML
	
	@Autowired
	private ISessionService sService;
	
	@Autowired
	private IFiliereService fService;
	
	//CRUD
	
	@PostMapping(value = "/add/{f_id}", produces = "application/json")
	public Session addSession(@RequestBody Session sIn, @PathVariable("f_id") int id) {
		Filiere filiere = fService.getFiliereById(id);
		sIn.setFiliere(filiere);
		return sService.addSession(sIn);
	}
	
	@DeleteMapping(value = "/del/{pId}")
	public void delSession(@PathVariable("pId") int id) {
		sService.delSessionById(id);
	}
	
	@PutMapping(value = "/update/{f_id}", produces = "application/json")
	public Session updateSession(@RequestBody Session sIn, @PathVariable("f_id") int id) {
		Filiere filiere = fService.getFiliereById(id);
		sIn.setFiliere(filiere);
		return sService.updateSession(sIn);
	}
	
	@GetMapping(value = "/getAll", produces = "application/json")
	public List<Session> getAllSession(){
		return sService.getAllSession();
	}
	
	@GetMapping(value = "/getById/{pId}", produces = "application/json")
	public Session getSessionById(@PathVariable("pId") int id) {
		return sService.getSessionById(id);
	}

}
