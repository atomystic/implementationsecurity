package com.controleurs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Rapport;
import com.entity.Sujet;
import com.service.IRapportService;
import com.service.ISujetService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/rapport")
public class RapportRest {

	// transfo UML JAVA
	@Autowired
	private IRapportService rService;
	
	@Autowired
	private ISujetService sService;

	// add rapport
	@PostMapping(value = "/add/{sId}", produces = "application/json")
	public Rapport addRapport(@RequestBody Rapport r, @PathVariable("sId") int sId) {
		Sujet sIn = sService.getSujetById(sId);
		r.setSujet(sIn);
		return rService.addRapport(r);
	}

    //delete rapport
	@DeleteMapping(value = "/del/{pId}")
	public void delRapport(@PathVariable("pId") int id) {
		rService.delRapportById(id);
	}

	// getall rapport
	@GetMapping(value = "/getAll", produces = "application/json")
	public List<Rapport> getListe() {
		return rService.getAllRapport();
	}
	
	//get by id
	@GetMapping(value = "/getById/{pId}", produces="application/json")
	public Rapport getRapportById(@PathVariable("pId")int id) {
		return rService.getRapportById(id);
	}
	
	//update rapport
	@PutMapping(value = "/update/{sId}", produces="application/json")
	public Rapport updateRapport(@RequestBody Rapport r, @PathVariable("sId") int sId) {
		Sujet sIn = sService.getSujetById(sId);
		r.setSujet(sIn);
		return rService.updateRapport(r);
	}
}
