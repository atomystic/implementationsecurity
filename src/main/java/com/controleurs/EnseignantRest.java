package com.controleurs;


import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Enseignant;
import com.service.IEnseignantService;

@RestController
@RequestMapping ("/enseignant")
@CrossOrigin (origins = "*")
public class EnseignantRest {
	
	//transfo assoc uml en java
		@Autowired
		private IEnseignantService ensService;
		
		
		// ======= AJOUTER ENSEIGNANT =========
		@PostMapping(value = "/add", produces = "application/json")
		public Enseignant addEnseignant(@RequestBody Enseignant ens) {
			// appel methode service
			return ensService.addEnseignant(ens);
		}

		// ======= SUPPRIMER ENSEIGNANT =========
		@DeleteMapping(value = "/del/{id}")
		public void delEnseignantById(@PathVariable("id") int id) {
			// appel methode service
			ensService.delEnseignantById(id);
			
		}

		// ======= LISTE DES ENSEIGNANT =========
		@GetMapping(value = "/getAll", produces = "application/json")
		public List<Enseignant> getAllEnseignant() {
			// appel methode service
			return ensService.getAllEnseignant();
		}

		// ======= RECHERCHER ENSEIGNANT =========
		@GetMapping(value = "/getById/{id}", produces = "application/json")
		public Enseignant getEnseignantById(@PathVariable("id") int id) {
			// appel methode service
			
			return ensService.getEnseignantById(id);
		}

		// ======= MODIFIER ENSEIGNANT =========
		@PutMapping(value = "/update", produces = "application/json")
		public Enseignant updateEnseignant(@RequestBody Enseignant ens) {
			// appel methode service
			return ensService.updateEnseignant(ens);
		}
}
