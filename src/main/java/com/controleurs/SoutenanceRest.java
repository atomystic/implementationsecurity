package com.controleurs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Enseignant;
import com.entity.Session;
import com.entity.Soutenance;
import com.entity.Sujet;
import com.service.IEnseignantService;
import com.service.ISessionService;
import com.service.ISoutenanceService;
import com.service.ISujetService;


@RestController
@RequestMapping("/soutenance")
@CrossOrigin(origins = "*")
public class SoutenanceRest {
	
	@Autowired
	private ISoutenanceService sService;
	
	@Autowired
	private ISessionService sessionService;
	
	@Autowired
	private ISujetService sujetService;
		
	// Add
	@PostMapping(value = "/add/{sessionId}/{sujetId}", produces = "application/json")
	public Soutenance addSoutenance(@RequestBody Soutenance soutenance, @PathVariable("sessionId")int sessionId, 
			@PathVariable("sujetId")int sujetId) {
		Session session = sessionService.getSessionById(sessionId);
		Sujet sujet = sujetService.getSujetById(sujetId);
		soutenance.setSession(session);
		soutenance.setSujet(sujet);
		return sService.addSoutenance(soutenance);
	}
	
	// Delete
	@DeleteMapping(value = "/del/{pId}")
	public void delSoutenanceById(@PathVariable("pId") int id) {
		sService.delSoutenanceById(id);
	}
	
	// getAll
	@GetMapping(value = "/getAll", produces = "application/json")
	public List<Soutenance> getAllSoutenance(){
		return sService.getAllSoutenance();
		}
	
	// getById
	@GetMapping(value = "/getById/{pId}", produces = "application/json")
	public Soutenance getSoutenanceById(@PathVariable("pId") int id) {
		return sService.getSoutenanceById(id);
	}

	// update
	@PutMapping(value ="/update/{sessionId}/{sujetId}", produces = "application/json")
	public Soutenance updateSoutenance(@RequestBody Soutenance soutenance, @PathVariable("sessionId")int sessionId, 
			@PathVariable("sujetId")int sujetId) {
		Session session = sessionService.getSessionById(sessionId);
		Sujet sujet = sujetService.getSujetById(sujetId);
		soutenance.setSession(session);
		soutenance.setSujet(sujet);
		return sService.updateSoutenance(soutenance);
	}
	
	// getListeSoutenance
	@GetMapping(value = "/findByEnseignant/{id}", produces = "application/json")
	public List<Soutenance> findSoutenanceByEnseignant(@PathVariable("id") int id){
		return sService.findSoutenanceByEnseignant(id);
	}
}
