package com.controleurs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.ERole;
import com.entity.Role;
import com.entity.Utilisateur;
import com.repository.IutilisateurDao;
import com.springjwt.payload.request.LoginRequest;
import com.springjwt.payload.request.SignupRequest;
import com.springjwt.payload.response.JwtResponse;
import com.springjwt.payload.response.MessageResponse;
import com.springjwt.repository.IRoleDao;
import com.springjwt.security.jwt.JwtUtils;
import com.springjwt.security.services.DetailsUtilisateurImpl;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	IutilisateurDao iUtilisateurDao;

	@Autowired
	IRoleDao iRoleDao;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		
		
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getLogin(), loginRequest.getMdp()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		DetailsUtilisateurImpl detailUtilisateur = (DetailsUtilisateurImpl) authentication.getPrincipal();
		System.out.println("=========================================================="+ detailUtilisateur.getAuthorities());
		List<String> roles = detailUtilisateur.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(
				new JwtResponse(jwt,detailUtilisateur.getId(), detailUtilisateur.getUsername(), roles));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		
		if (iUtilisateurDao.existsByLogin(signUpRequest.getLogin())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: L'identifiant est déjà pris!"));
		}


		// Create new user's account
		Utilisateur utilisateur = new Utilisateur(signUpRequest.getLogin(),encoder.encode(signUpRequest.getMdp()));
          
		Set<String> strRoles = signUpRequest.getRole();
		
		List<Role> roles = new ArrayList<>();
		
		if (strRoles == null) {
			Role userRole = iRoleDao.findByRoleName(ERole.ROLE_ETUDIANT) // à corrigeer
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
			
		} 
		
		
		else {
			strRoles.forEach(role -> {
				switch (role) {
				
			
				case "admin":
			
					Role adminRole = iRoleDao.findByRoleName(ERole.ROLE_ADMIN) // à corriger
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);
					break;
					
				case "etudiant":

					Role etudiantRole = iRoleDao.findByRoleName(ERole.ROLE_ETUDIANT) // à corriger
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(etudiantRole);
					break;
				default:
					Role enseignantRole = iRoleDao.findByRoleName(ERole.ROLE_ENSEIGNANT)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(enseignantRole);

				}
			});
	}

		utilisateur.setRoles(roles);
		
	Utilisateur u =	iUtilisateurDao.save(utilisateur);
	
		System.out.println("=============================="+u.getRoles());
		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}
}