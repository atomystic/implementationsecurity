package com.springjwt.payload.request;

import java.util.Set;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


public class SignupRequest {
    @NotBlank
    @Size(min = 3, max = 20)
    private String login;
    
    private String email;
    
 
    private Set<String> role;
    
    @NotBlank
    @Size(min = 6, max = 40)
    private String mdp;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Set<String> getRole() {
		return role;
	}

	public void setRole(Set<String> role) {
		this.role = role;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "SignupRequest [login=" + login + ", email=" + email + ", role=" + role + ", mdp=" + mdp + "]";
	}




    
    
}
