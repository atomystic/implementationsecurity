package com.springjwt.security.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


import com.entity.Utilisateur;
import com.repository.IutilisateurDao;

@Service
public class DetailsUtilisateurServiceImpl implements UserDetailsService {
	@Autowired
	IutilisateurDao iDao;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Utilisateur utilisateur = iDao.findByLogin(username)
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

		return DetailsUtilisateurImpl.build(utilisateur);
	}

}
