package com.springjwt.security.services;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import com.entity.Utilisateur;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class DetailsUtilisateurImpl implements UserDetails {
	
	
	private static final long serialVersionUID = 1L;

	private long id;

	private String login;

	@JsonIgnore
	private String mdp;
	
	private Collection<? extends GrantedAuthority> authorities;
	

	public DetailsUtilisateurImpl(long id, String login, String mdp,
			Collection<? extends GrantedAuthority> authorities) {
		this.id = id;
		this.login = login;
		this.mdp = mdp;
		this.authorities = authorities;
	}
	
	public static DetailsUtilisateurImpl build(Utilisateur utilisateur) {
		List<GrantedAuthority> authorities = utilisateur.getRoles().stream()
				.map(role -> new SimpleGrantedAuthority(role.getRoleName().name())) 	//.name
				.collect(Collectors.toList());

		return new DetailsUtilisateurImpl(
				utilisateur.getId(), 
				utilisateur.getLogin(), 
				utilisateur.getMdp(), 
				authorities);
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return authorities;
	}

	public long getId() {
		return id;
	}

	@Override
	public String getPassword() {
		
		return mdp;
	}

	@Override
	public String getUsername() {

		return login;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		DetailsUtilisateurImpl user = (DetailsUtilisateurImpl) o;
		return Objects.equals(id, user.id);
	}


}
