package com.springjwt.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.ERole;
import com.entity.Role;


@Repository
public interface IRoleDao extends JpaRepository<Role, Integer> {
	  
		Optional<Role> findByRoleName(ERole roleEtudiant);
}
