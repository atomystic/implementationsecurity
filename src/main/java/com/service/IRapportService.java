package com.service;

import java.util.List;

import com.entity.Rapport;

public interface IRapportService {
	
	public Rapport addRapport(Rapport r);

	public void delRapportById(int id);

	public List<Rapport> getAllRapport();

	public Rapport getRapportById(int id);

	public Rapport updateRapport(Rapport r);

}
