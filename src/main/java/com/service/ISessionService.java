package com.service;

import java.util.List;

import com.entity.Session;

public interface ISessionService {
	
	public Session addSession (Session session);
	
	public void delSessionById(int id);
	
	public List<Session> getAllSession();
	
	public Session getSessionById(int id);
	
	public Session updateSession(Session session);
		

}
