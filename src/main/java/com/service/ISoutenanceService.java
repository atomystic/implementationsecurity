package com.service;

import java.util.List;

import com.entity.Soutenance;

public interface ISoutenanceService {
	
	public Soutenance addSoutenance (Soutenance soutenance);
	
	public void delSoutenanceById (int id);
	
	public List<Soutenance> getAllSoutenance ();
	
	public Soutenance getSoutenanceById (int id);
	
	public Soutenance updateSoutenance (Soutenance soutenance);

	public List<Soutenance> findSoutenanceByEnseignant(int id);

}
