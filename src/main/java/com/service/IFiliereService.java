package com.service;

import java.util.List;

import com.entity.Filiere;

public interface IFiliereService {
	
	public Filiere addFiliere(Filiere fIn);
	
	public void delFiliereById(int id);
	
	public List<Filiere> getAllFiliere();
	
	public Filiere getFiliereById(int id);
	
	public Filiere updateFiliere(Filiere fIn);

}
