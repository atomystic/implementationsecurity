package com.service;

import java.util.List;

import com.entity.Organisme;

public interface IOrganismeService {
	
	public Organisme addOrganisme (Organisme organisme);
	
	public void delOrganismeById (int id);
	
	public List<Organisme> getAllOrganisme ();
	
	public Organisme getOrganismeById (int id);
	
	public Organisme updateOrganisme (Organisme organisme);

	}


