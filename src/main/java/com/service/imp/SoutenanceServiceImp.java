package com.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.ast.OpInc;
import org.springframework.stereotype.Service;

import com.entity.Enseignant;
import com.entity.Soutenance;
import com.entity.Sujet;
import com.repository.IEnseignantDao;
import com.repository.ISoutenanceDao;
import com.repository.ISujetDao;
import com.service.ISoutenanceService;

@Service
public class SoutenanceServiceImp implements ISoutenanceService {

	@Autowired
	private ISoutenanceDao sDao;

	@Autowired
	private IEnseignantDao ensDao;

	@Override
	public Soutenance addSoutenance(Soutenance soutenance) {
		return sDao.save(soutenance);
	}

	@Override
	public void delSoutenanceById(int id) {
		sDao.deleteById(id);
	}

	@Override
	public List<Soutenance> getAllSoutenance() {
		return sDao.findAll();
	}

	@Override
	public Soutenance getSoutenanceById(int id) {
		Optional<Soutenance> sOut = sDao.findById(id);
		return sOut.get();
	}

	@Override
	public Soutenance updateSoutenance(Soutenance soutenance) {
		return sDao.save(soutenance);
	}

	@Override
	public List<Soutenance> findSoutenanceByEnseignant(int id) {

		// recuperer l'enseignant
		Optional<Enseignant> enseignant = ensDao.findById(id);

		Enseignant ensOut = enseignant.get();

		// creer liste de soutenance vide
		List<Soutenance> liste = new ArrayList<Soutenance>();
		
		// recuperer la liste des sujets qu'il preside
		List<Sujet> sujPresides = ensOut.getPresides();

		// recuperer pour chaque sujet la soutenance et la stocker dans la liste
		for (Sujet sujet : sujPresides) {

			liste.add(sujet.getSoutenance());

		}

		// recuperer la liste des sujets qu'il rapporte
		List<Sujet> sujRapportes = ensOut.getRapportes();

		// recuperer pour chaque sujet la soutenance et la stocker dans la liste
		for (Sujet sujet : sujRapportes) {

			liste.add(sujet.getSoutenance());

		}

		// recuperer la liste des sujets qu'il rapporte
		List<Sujet> sujEncadres = ensOut.getEncadres();

		// recuperer pour chaque sujet la soutenance et la stocker dans la liste
		for (Sujet sujet : sujEncadres) {

			liste.add(sujet.getSoutenance());

		}

		return liste;
	}

}
