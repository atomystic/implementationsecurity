package com.service.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Filiere;
import com.repository.IFiliereDao;
import com.service.IFiliereService;

@Service
public class FiliereServiceImpl implements IFiliereService{
	
	@Autowired
	private IFiliereDao fDao;

	@Override
	public Filiere addFiliere(Filiere fIn) {
		return fDao.save(fIn);
	}

	@Override
	public void delFiliereById(int id) {
		fDao.deleteById(id);
	}

	@Override
	public List<Filiere> getAllFiliere() {
		return fDao.findAll();
	}

	@Override
	public Filiere getFiliereById(int id) {
		
		Optional<Filiere> ops = fDao.findById(id);
		
		return ops.get();
	}

	@Override
	public Filiere updateFiliere(Filiere fIn) {
		return fDao.save(fIn);
	}

}
