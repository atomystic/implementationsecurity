package com.service.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Tuteur;
import com.repository.ITuteurDao;
import com.service.ITuteurService;

@Service
public class TuteurServiceImpl implements ITuteurService{

	//transfo assoc uml en java
	@Autowired
	private ITuteurDao tutDao;
	
	@Override
	public Tuteur addTuteur(Tuteur tut) {
		// appel methode dao
		return tutDao.save(tut);
	}

	@Override
	public void delTuteurById(int id) {
		// appel methode dao
		tutDao.deleteById(id);
		
	}

	@Override
	public List<Tuteur> getAllTuteur() {
		// appel methode dao
		return tutDao.findAll();
	}

	@Override
	public Tuteur getTuteurById(int id) {
		// appel methode dao
		Optional<Tuteur> tuteur=tutDao.findById(id);
		return tuteur.get();
	}

	@Override
	public Tuteur updateTuteur(Tuteur tut) {
		// appel methode dao
		return tutDao.save(tut);
	}

	@Override
	public List<Tuteur> findTuteurByOrganisme(int id) {
		// appel methode Dao
		return tutDao.findTuteurByOrganisme(id);
	}

}
