package com.service.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Rapport;
import com.repository.IRapportDao;
import com.service.IRapportService;

@Service
public class RapportServiceImpl implements IRapportService{

	//transformation UML JAVA
	@Autowired
	private IRapportDao rDao;
	
	
	
	@Override
	public Rapport addRapport(Rapport r) {
		return rDao.save(r);
	}

	@Override
	public void delRapportById(int id) {
		rDao.deleteById(id);
	}

	@Override
	public List<Rapport> getAllRapport() {
		return rDao.findAll();
	}

	@Override
	public Rapport getRapportById(int id) {
		Optional<Rapport> rapOps = rDao.findById(id);
		return rapOps.get(); 
	}

	@Override
	public Rapport updateRapport(Rapport r) {
		return rDao.save(r);
	}

}
