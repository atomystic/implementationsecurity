package com.service.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Organisme;
import com.repository.IOrganismeDao;
import com.service.IOrganismeService;


@Service
public class OrganismeServiceImp implements IOrganismeService {
	
	@Autowired
	private IOrganismeDao oDao;

	@Override
	public Organisme addOrganisme(Organisme organisme) {
		return oDao.save(organisme);
	}

	@Override
	public void delOrganismeById(int id) {
		oDao.deleteById(id);
		
	}

	@Override
	public List<Organisme> getAllOrganisme() {
		return oDao.findAll();
	}

	@Override
	public Organisme getOrganismeById(int id) {
		Optional<Organisme> OOut = oDao.findById(id);
		return OOut.get();
	}

	@Override
	public Organisme updateOrganisme(Organisme organisme) {
		return oDao.save(organisme);
	}

}
