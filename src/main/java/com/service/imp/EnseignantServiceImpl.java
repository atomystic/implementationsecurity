package com.service.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Enseignant;
import com.repository.IEnseignantDao;
import com.service.IEnseignantService;

@Service
public class EnseignantServiceImpl implements IEnseignantService{
	
	//transfo assoc uml en java
	@Autowired
	private IEnseignantDao ensDao;
	
	
	@Override
	public Enseignant addEnseignant(Enseignant ens) {
		// appel methode dao
		return ensDao.save(ens);
	}

	@Override
	public void delEnseignantById(int id) {
		// appel methode dao
		ensDao.deleteById(id);
		
	}

	@Override
	public List<Enseignant> getAllEnseignant() {
		// appel methode dao
		return ensDao.findAll();
	}

	@Override
	public Enseignant getEnseignantById(int id) {
		// appel methode dao
		Optional<Enseignant> enseignant=ensDao.findById(id);
		return enseignant.get();
	}

	@Override
	public Enseignant updateEnseignant(Enseignant ens) {
		// appel methode dao
		return ensDao.save(ens);
	}

}
