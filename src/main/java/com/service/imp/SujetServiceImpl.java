package com.service.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Enseignant;
import com.entity.Sujet;
import com.repository.ISujetDao;
import com.service.ISujetService;

@Service
public class SujetServiceImpl implements ISujetService{

	//Association UML
	
	@Autowired
	private ISujetDao sDao;
	
	//CRUD
	@Override
	public Sujet addSujet(Sujet sIn) {
	
		return sDao.save(sIn);
	}

	@Override
	public void delSujetById(int id) {
		sDao.deleteById(id);	
	}

	@Override
	public List<Sujet> getAllSujet() {

		return sDao.findAll();
	}

	@Override
	public Sujet getSujetById(int id) {
		
		Optional<Sujet> ops = sDao.findById(id);
		
		return ops.get();
	}

	@Override
	public Sujet updateSujet(Sujet sIn) {
		return sDao.save(sIn);
	}

	
	//Méthodes métier
	@Override
	public List<Sujet> getSujetByValide(short valide) {
		return sDao.findByValide(valide);
	}

	@Override
	public List<Sujet> getSujetNonValide() {

		List<Sujet> nonValide = sDao.findByValide((short) 0);
		List<Sujet> valideUneFois = sDao.findByValide((short) 1);
		
		for(Sujet suj:nonValide) {
			valideUneFois.add(suj);
		}
		
		return valideUneFois;
	}

	@Override
	public Sujet validerSujet(Sujet sIn, Enseignant valideur) {
		
		if(sIn.getValide()==0) {
			sIn.setValide((short) 1);
			sIn.setValideur1(valideur);
			updateSujet(sIn)	;
		}else if (sIn.getValide()==1) {
			if(sIn.getValideur1().getId()!=valideur.getId()) {
				sIn.setValide((short) 2);
				sIn.setValideur2(valideur);
				updateSujet(sIn);
			}
		}
		
		return sIn;
	}

}
