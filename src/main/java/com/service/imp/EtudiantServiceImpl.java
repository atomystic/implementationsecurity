package com.service.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Etudiant;
import com.repository.IEtudiantDao;
import com.service.IEtudiantService;

@Service
public class EtudiantServiceImpl implements IEtudiantService{

	//transfo assoc uml en java
	@Autowired
	private IEtudiantDao etuDao;
	
	@Override
	public Etudiant addEtudiant(Etudiant etu) {
		// appel methode dao
		return etuDao.save(etu);
	}

	@Override
	public void delEtudiantById(int id) {
		// appel methode dao
		etuDao.deleteById(id);
		
	}

	@Override
	public List<Etudiant> getAllEtudiant() {
		// appel methode dao
		return etuDao.findAll();
	}

	@Override
	public Etudiant getEtudiantById(int id) {
		// appel methode dao
		Optional<Etudiant> etudiant=etuDao.findById(id);
		return etudiant.get();
	}

	@Override
	public Etudiant updateEtudiant(Etudiant etu) {
		// appel methode dao
		return etuDao.save(etu);
	}

	@Override
	public List<Etudiant> findEtudiantBySujet(int id) {
		// appel methode dao
		return etuDao.findEtudiantBySujet(id);
	}

}
