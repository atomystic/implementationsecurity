package com.service.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Session;
import com.repository.ISessionDao;
import com.service.ISessionService;

@Service
public class SessionServiceImp implements ISessionService {
	
	@Autowired
	private ISessionDao sDao;

	@Override
	public Session addSession(Session session) {
		return sDao.save(session);
	}

	@Override
	public void delSessionById(int id) {
		sDao.deleteById(id);
	}

	@Override
	public List<Session> getAllSession() {
		return sDao.findAll();
	}

	@Override
	public Session getSessionById(int id) {
		Optional<Session> SOut = sDao.findById(id);
		return SOut.get();
	}

	@Override
	public Session updateSession(Session session) {
		return sDao.save(session);
	}
	
	

}
