package com.service.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Admin;
import com.repository.IAdminDao;
import com.service.IAdminService;

@Service
public class AdminServiceImpl implements IAdminService{
	
	//Lien UML
	@Autowired
	private IAdminDao aDao;
	

	@Override
	public Admin addAdmin(Admin aIn) {
		return aDao.save(aIn);
	}

	@Override
	public void delAdminById(int id) {

		aDao.deleteById(id);
	}

	@Override
	public List<Admin> getAllAdmin() {
		return aDao.findAll();
	}

	@Override
	public Admin updateAdmin(Admin aIn) {
		return aDao.save(aIn);
	}

	@Override
	public Admin getAdminById(int id) {
		
		Optional<Admin> ops = aDao.findById(id);
		
		return ops.get();
	}

}
