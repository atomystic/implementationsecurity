package com.service;

import java.util.List;

import com.entity.Tuteur;

public interface ITuteurService {

	public Tuteur addTuteur(Tuteur tut);
	
	public void delTuteurById(int id);
	
	public List<Tuteur> getAllTuteur();
	
	public Tuteur getTuteurById(int id);
	
	public Tuteur updateTuteur(Tuteur tut);
	
	public List<Tuteur> findTuteurByOrganisme(int id);
}
