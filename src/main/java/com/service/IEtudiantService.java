package com.service;

import java.util.List;

import com.entity.Etudiant;

public interface IEtudiantService {

	public Etudiant addEtudiant(Etudiant etu);
	
	public void delEtudiantById(int id);
	
	public List<Etudiant> getAllEtudiant();
	
	public Etudiant getEtudiantById(int id);
	
	public Etudiant updateEtudiant(Etudiant etu);
	
	public List<Etudiant> findEtudiantBySujet(int id);
	
}
