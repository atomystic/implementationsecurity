package com.service;

import java.util.List;

import com.entity.Admin;

public interface IAdminService {

	public Admin addAdmin(Admin aIn);
	
	public void delAdminById(int id);
	
	public List<Admin> getAllAdmin();
	
	public Admin updateAdmin(Admin aIn);
	
	public Admin getAdminById(int id);
	
}
