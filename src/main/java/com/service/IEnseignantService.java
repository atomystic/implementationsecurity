package com.service;

import java.util.List;

import com.entity.Enseignant;


public interface IEnseignantService {

	public Enseignant addEnseignant(Enseignant ens);
	
	public void delEnseignantById(int id);
	
	public List<Enseignant> getAllEnseignant();
	
	public Enseignant getEnseignantById(int id);
	
	public Enseignant updateEnseignant(Enseignant ens);
	
}
