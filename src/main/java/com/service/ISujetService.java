package com.service;

import java.util.List;

import com.entity.Enseignant;
import com.entity.Sujet;

public interface ISujetService {
	
	//CRUD
	
	public Sujet addSujet(Sujet sIn);
	
	public void delSujetById(int id);
	
	public List<Sujet> getAllSujet();
	
	public Sujet getSujetById(int id);
	
	public Sujet updateSujet(Sujet sIn);
	
	
	//Méthodes métier
	
	public List<Sujet> getSujetByValide(short valide);
	
	public List<Sujet> getSujetNonValide();
	
	public Sujet validerSujet(Sujet sIn, Enseignant valideur);

}
