package com.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "sujets")
public class Sujet implements Serializable {

	// Attributs

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_sujet", length = 36)
	private int id;
	private String titre;
	private String def;
	private String specialite;
	private short valide;

	@OneToOne(mappedBy = "sujet", cascade = CascadeType.REMOVE)
	private Soutenance soutenance;

	@OneToMany(mappedBy = "sujet", cascade = CascadeType.REMOVE)
	private List<Rapport> rapports;

	@OneToOne
	@JoinColumn(name = "tuteur_id", referencedColumnName = "id_utilisateur")
	private Tuteur tuteur;

	@OneToMany(mappedBy = "sujet")
	@JsonIgnore
	private List<Etudiant> etudiants;

	@ManyToOne
	@JoinColumn(name = "valideur1_id", referencedColumnName = "id_utilisateur")
	private Enseignant valideur1;

	@ManyToOne
	@JoinColumn(name = "valideur2_id", referencedColumnName = "id_utilisateur")
	private Enseignant valideur2;

	@ManyToOne
	@JoinColumn(name = "encadrant_id", referencedColumnName = "id_utilisateur")
	private Enseignant encadrant;

	@ManyToOne
	@JoinColumn(name = "filiere_id", referencedColumnName = "id_filiere")
	private Filiere filiere;

	@ManyToOne
	@JoinColumn(name = "president_id", referencedColumnName = "id_utilisateur")
	private Enseignant president;

	@ManyToOne
	@JoinColumn(name = "rapporteur_id", referencedColumnName = "id_utilisateur")
	private Enseignant rapporteur;

	// Constructeurs
	public Sujet() {
		super();
	}

	public Sujet(String titre, String def, String specialite, short valide) {
		super();
		this.titre = titre;
		this.def = def;
		this.specialite = specialite;
		this.valide = valide;
	}

	public Sujet(int id, String titre, String def, String specialite, short valide) {
		super();
		this.id = id;
		this.titre = titre;
		this.def = def;
		this.specialite = specialite;
		this.valide = valide;
	}

	// Getters et setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getDef() {
		return def;
	}

	public void setDef(String def) {
		this.def = def;
	}

	public short getValide() {
		return valide;
	}

	public void setValide(short valide) {
		this.valide = valide;
	}

	public String getSpecialite() {
		return specialite;
	}

	public void setSpecialite(String specialite) {
		this.specialite = specialite;
	}
	@JsonIgnoreProperties({"sujet"})
	public Soutenance getSoutenance() {
		return soutenance;
	}

	public void setSoutenance(Soutenance soutenance) {
		this.soutenance = soutenance;
	}
	@JsonIgnoreProperties({"sujet"})
	public List<Rapport> getRapports() {
		return rapports;
	}

	public void setRapports(List<Rapport> rapports) {
		this.rapports = rapports;
	}

	@JsonIgnoreProperties({"sujet"})
	public Tuteur getTuteur() {
		return tuteur;
	}

	public void setTuteur(Tuteur tuteur) {
		this.tuteur = tuteur;
	}

	public List<Etudiant> getEtudiants() {
		return etudiants;
	}

	public void setEtudiants(List<Etudiant> etudiants) {
		this.etudiants = etudiants;
	}

	public Enseignant getValideur1() {
		return valideur1;
	}

	public void setValideur1(Enseignant valideur1) {
		this.valideur1 = valideur1;
	}

	public Enseignant getValideur2() {
		return valideur2;
	}

	public void setValideur2(Enseignant valideur2) {
		this.valideur2 = valideur2;
	}

	@JsonIgnoreProperties({ "encadres", "presides", "rapportes" })
	public Enseignant getEncadrant() {
		return encadrant;
	}

	public void setEncadrant(Enseignant encadrant) {
		this.encadrant = encadrant;
	}

	public Filiere getFiliere() {
		return filiere;
	}

	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}

	@JsonIgnoreProperties({ "encadres", "presides", "rapportes" })
	public Enseignant getPresident() {
		return president;
	}

	public void setPresident(Enseignant president) {
		this.president = president;
	}

	@JsonIgnoreProperties({ "encadres", "presides", "rapportes" })
	public Enseignant getRapporteur() {
		return rapporteur;
	}

	public void setRapporteur(Enseignant rapporteur) {
		this.rapporteur = rapporteur;
	}

	// toString
	@Override
	public String toString() {
		return "Sujet [id=" + id + ", titre=" + titre + ", def=" + def + ", specialite=" + specialite + ", valide="
				+ valide + "]";
	}

}
