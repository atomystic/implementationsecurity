package com.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "ADM")
public class Admin extends Utilisateur {

	// ==== ATTRIBUTS ====

	private String poste;
	
	// ==== CONSTRUCTEURS ====

	// avec id
	public Admin(int id, String nom, String prenom, String login, String mdp, boolean active, String poste) {
		super(id, nom, prenom, login, mdp, active);
		this.poste = poste;
	}

	// sans id
	public Admin(String nom, String prenom, String login, String mdp, boolean active, String poste) {
		super(nom, prenom, login, mdp, active);
		this.poste = poste;
	}

	// vide
	public Admin() {
		super();
	}

	// ==== GETTERS & SETTERS ====

	public String getPoste() {
		return poste;
	}

	public void setPoste(String poste) {
		this.poste = poste;
	}

	// ==== toString ====

	@Override
	public String toString() {
		return "Admin [poste=" + poste + "]";
	}

}
