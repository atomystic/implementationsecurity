package com.entity;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@DiscriminatorValue(value = "ENS")
public class Enseignant extends Utilisateur {

	// ==== ATTRIBUTS ====

	private int matricule;
	private String specialite;

	@Embedded
	private Adresse adresse;

	@OneToMany(mappedBy = "valideur1")
	@JsonIgnore
	private List<Sujet> valides1;

	@OneToMany(mappedBy = "valideur2")
	@JsonIgnore
	private List<Sujet> valides2;

	@OneToMany(mappedBy = "encadrant")
	@JsonIgnore
	private List<Sujet> encadres;

	@OneToMany(mappedBy = "president")
	@JsonIgnore
	private List<Sujet> presides;

	@OneToMany(mappedBy = "rapporteur")
	@JsonIgnore
	private List<Sujet> rapportes;

	// ==== CONSTRUCTEURS ====

	// avec id
	public Enseignant(int id, String nom, String prenom, String login, String mdp, boolean active, int matricule,
			String specialite) {
		super(id, nom, prenom, login, mdp, active);
		this.matricule = matricule;
		this.specialite = specialite;
	}

	// sans id
	public Enseignant(String nom, String prenom, String login, String mdp, boolean active, int matricule,
			String specialite) {
		super(nom, prenom, login, mdp, active);
		this.matricule = matricule;
		this.specialite = specialite;
	}

	// vide
	public Enseignant() {
		super();
	}

	// ==== GETTERS & SETTERS ====

	public int getMatricule() {
		return matricule;
	}

	public void setMatricule(int matricule) {
		this.matricule = matricule;
	}

	public String getSpecialite() {
		return specialite;
	}

	public void setSpecialite(String specialite) {
		this.specialite = specialite;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public List<Sujet> getValides1() {
		return valides1;
	}

	public void setValides1(List<Sujet> valides1) {
		this.valides1 = valides1;
	}

	public List<Sujet> getValides2() {
		return valides2;
	}

	public void setValides2(List<Sujet> valides2) {
		this.valides2 = valides2;
	}

	public List<Sujet> getEncadres() {
		return encadres;
	}

	public void setEncadres(List<Sujet> encadres) {
		this.encadres = encadres;
	}

	public List<Sujet> getPresides() {
		return presides;
	}

	public void setPresides(List<Sujet> presides) {
		this.presides = presides;
	}

	public List<Sujet> getRapportes() {
		return rapportes;
	}

	public void setRapportes(List<Sujet> rapportes) {
		this.rapportes = rapportes;
	}

	// ==== toString ====

	@Override
	public String toString() {
		return "Enseignant [matricule=" + matricule + ", specialite=" + specialite + " ] ";
	}

}
