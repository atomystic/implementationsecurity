package com.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "soutenances")
public class Soutenance implements Serializable {

	// Attributs
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_soutenance", length = 36)
	private int id;
	private Date date;
	private String salle;

	@ManyToOne
	@JoinColumn(name = "session_id", referencedColumnName = "id_session")
	private Session session;

	@OneToOne
	@JoinColumn(name = "sujet_id", referencedColumnName = "id_sujet")
	private Sujet sujet;

	// Constructeur avec id
	public Soutenance(int id, Date date, String salle) {
		super();
		this.id = id;
		this.date = date;
		this.salle = salle;

	}

	// Constructeur sans id
	public Soutenance(Date date, String salle) {
		super();
		this.date = date;
		this.salle = salle;

	}

	// Constructeur vide
	public Soutenance() {
		super();
	}

	// Getters and setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getSalle() {
		return salle;
	}

	public void setSalle(String salle) {
		this.salle = salle;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public Sujet getSujet() {
		return sujet;
	}

	public void setSujet(Sujet sujet) {
		this.sujet = sujet;
	}

	// Méthode toString
	@Override
	public String toString() {
		return "Soutenance [id=" + id + ", date=" + date + ", salle=" + salle + "]";
	}

}
