package com.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Set;


import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "utilisateurs")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "user_type")
public  class Utilisateur implements Serializable {

	// ==== ATTRIBUTS ====
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_utilisateur", length = 36)
	protected int id;
	protected String nom;
	protected String prenom;
	protected String login;
	protected String mdp;
	protected boolean active;
	protected String email;

	@ManyToMany(mappedBy = "utilisateurs")
	protected List<Role> roles;

	// ==== CONSTRUCTEURS ====

	// avec id
	public Utilisateur(int id, String nom, String prenom, String login, String mdp, boolean active) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.login = login;
		this.mdp = mdp;
		this.active = active;
	}



	public Utilisateur(String login, String mdp, String email, List<Role> roles) {
		
		this.login = login;
		this.mdp = mdp;
		this.email = email;
		this.roles = roles;
	}



	// sans id
	public Utilisateur(String nom, String prenom, String login, String mdp, boolean active) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.login = login;
		this.mdp = mdp;
		this.active = active;
	}

	//signuprequest
	public Utilisateur(String login, String mdp) {
		super();
		this.login = login;
		this.mdp = mdp;
	}
	
	
// ne pas enlever
	public Utilisateur(String login, String mdp, String email) {
		super();
		this.login = login;
		this.mdp = mdp;
		this.email = email;
	}



	// vide
	public Utilisateur() {
		super();
	}

	// ==== GETTERS & SETTERS ====

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	@JsonIgnoreProperties({"utilisateurs"})
	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	// ==== toString ====
	@Override
	public String toString() {
		return "Utilisateur [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", login=" + login + ", mdp=" + mdp
				+ ", active=" + active + "]";
	}



}
