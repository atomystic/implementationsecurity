package com.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "organismes")
public class Organisme implements Serializable {

	// attributs

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_organisme", length = 36)
	private int id;
	private String nom;
	private String mail;
	private String tel;

	@Embedded
	private Adresse adresse;

	@OneToMany(mappedBy = "organisme", cascade = CascadeType.REMOVE)
	@JsonIgnore
	private List<Tuteur> tuteurs;

	// constructeur vide
	public Organisme() {
		super();

	}

//constructeur sans ID
	public Organisme(String nom, String mail, String tel) {
		super();
		this.nom = nom;
		this.mail = mail;
		this.tel = tel;
	}

	// constructeur plein
	public Organisme(int id, String nom, String mail, String tel) {
		super();
		this.id = id;
		this.nom = nom;
		this.mail = mail;
		this.tel = tel;
	}

	// getters setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public List<Tuteur> getTuteurs() {
		return tuteurs;
	}

	public void setTuteurs(List<Tuteur> tuteurs) {
		this.tuteurs = tuteurs;
	}

	// to string
	@Override
	public String toString() {
		return "Organisme [id=" + id + ", nom=" + nom + ", mail=" + mail + ", tel=" + tel + "]";
	}

}
