package com.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue(value = "ETU")
public class Etudiant extends Utilisateur {

	// ==== ATTRIBUTS ====

	private String identification;
	private Date dateNaissance;
	private int note;
	private String mention;
	private String remarque;

	@Embedded
	private Adresse adresse;

	@ManyToOne
	@JoinColumn(name = "filiere_id", referencedColumnName = "id_filiere")
	private Filiere filiere;

	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "sujet_id", referencedColumnName = "id_sujet")
	private Sujet sujet;

	// ==== CONSTRUCTEURS ====

	// avec id
	public Etudiant(int id, String nom, String prenom, String login, String mdp, boolean active, String identification,
			Date dateNaissance, int note, String mention, String remarque) {
		super(id, nom, prenom, login, mdp, active);
		this.identification = identification;
		this.dateNaissance = dateNaissance;
		this.note = note;
		this.mention = mention;
		this.remarque = remarque;
	}

	// sans id
	public Etudiant(String nom, String prenom, String login, String mdp, boolean active, String identification,
			Date dateNaissance, int note, String mention, String remarque) {
		super(nom, prenom, login, mdp, active);
		this.identification = identification;
		this.dateNaissance = dateNaissance;
		this.note = note;
		this.mention = mention;
		this.remarque = remarque;
	}

	// vide
	public Etudiant() {
		super();
	}

	// ==== GETTERS & SETTERS ====
	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public int getNote() {
		return note;
	}

	public void setNote(int note) {
		this.note = note;
	}

	public String getMention() {
		return mention;
	}

	public void setMention(String mention) {
		this.mention = mention;
	}

	public String getRemarque() {
		return remarque;
	}

	public void setRemarque(String remarque) {
		this.remarque = remarque;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public Filiere getFiliere() {
		return filiere;
	}

	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}

	public Sujet getSujet() {
		return sujet;
	}

	public void setSujet(Sujet sujet) {
		this.sujet = sujet;
	}

	// ==== toString ====

	@Override
	public String toString() {
		return "Etudiant [identification=" + identification + ", dateNaissance=" + dateNaissance + ", note=" + note
				+ ", mention=" + mention + ", remarque=" + remarque + "]";
	}

}
