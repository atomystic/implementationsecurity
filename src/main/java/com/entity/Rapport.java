package com.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "rapports")
public class Rapport implements Serializable {

	// attributs

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_rapport", length = 36)
	private int id;
	private String avancement;
	private Date dateReu;
	private String objectifs;

	@ManyToOne
	@JoinColumn(name = "sujet_id", referencedColumnName = "id_sujet")
	private Sujet sujet;

	// constructeur vide
	public Rapport() {
		super();
	}

// constructeur sans id
	public Rapport(String avancement, Date dateReu, String objectifs) {
		super();
		this.avancement = avancement;
		this.dateReu = dateReu;
		this.objectifs = objectifs;
	}

	// constructeur plein
	public Rapport(int id, String avancement, Date dateReu, String objectifs) {
		super();
		this.id = id;
		this.avancement = avancement;
		this.dateReu = dateReu;
		this.objectifs = objectifs;
	}

//getters setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAvancement() {
		return avancement;
	}

	public void setAvancement(String avancement) {
		this.avancement = avancement;
	}

	public Date getDateReu() {
		return dateReu;
	}

	public void setDateReu(Date dateReu) {
		this.dateReu = dateReu;
	}

	public String getObjectifs() {
		return objectifs;
	}

	public void setObjectifs(String objectifs) {
		this.objectifs = objectifs;
	}

	public Sujet getSujet() {
		return sujet;
	}

	public void setSujet(Sujet sujet) {
		this.sujet = sujet;
	}

	// to string
	@Override
	public String toString() {
		return "Rapport [id=" + id + ", avancement=" + avancement + ", dateReu=" + dateReu + ", objectifs=" + objectifs
				+ "]";
	}

}
