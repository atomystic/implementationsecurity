package com.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@DiscriminatorValue(value = "TUT")
public class Tuteur extends Utilisateur {

	// ==== ATTRIBUTS ====
	private String fonction;

	@OneToOne(mappedBy = "tuteur", cascade = CascadeType.REMOVE)
	//@JsonIgnore
	private Sujet sujet;

	@ManyToOne
	@JoinColumn(name = "organisme_id", referencedColumnName = "id_organisme")
	private Organisme organisme;

	// ==== CONSTRUCTEURS ====

	// avec id
	public Tuteur(int id, String nom, String prenom, String login, String mdp, boolean active, String fonction) {
		super(id, nom, prenom, login, mdp, active);
		this.fonction = fonction;
	}

	// sans id
	public Tuteur(String nom, String prenom, String login, String mdp, boolean active, String fonction) {
		super(nom, prenom, login, mdp, active);
		this.fonction = fonction;
	}

	// vide
	public Tuteur() {
		super();
	}

	// ==== GETTERS & SETTERS ====

	public String getFonction() {
		return fonction;
	}

	public void setFonction(String fonction) {
		this.fonction = fonction;
	}
	@JsonIgnoreProperties({"tuteur"})
	public Sujet getSujet() {
		return sujet;
	}

	public void setSujet(Sujet sujet) {
		this.sujet = sujet;
	}

	public Organisme getOrganisme() {
		return organisme;
	}

	public void setOrganisme(Organisme organisme) {
		this.organisme = organisme;
	}

	// ==== toString ====
	@Override
	public String toString() {
		return "Tuteur [fonction=" + fonction + "]";
	}

}
