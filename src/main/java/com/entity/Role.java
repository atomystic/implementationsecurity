package com.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



@Entity
@Table(name = "roles")
public class Role implements Serializable {

	// Déclaration des attributs
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Enumerated(EnumType.STRING)
	@Column(length = 20)
	private ERole roleName;

	
	@ManyToMany
	@JoinTable(name = "Role_Utilisateur", joinColumns = @JoinColumn(name = "role_id"), inverseJoinColumns = @JoinColumn(name = "utilisateur_id"))
	private List<Utilisateur> utilisateurs;

	// Déclaration des constructeurs
	// Constructeur vide
	public Role() {

	}

	// Constructeur sans id
	public Role(ERole roleName) {
		super();
		this.roleName = roleName;
	}

	// constructeur global
	public Role(int id, ERole roleName) {

		this.id = id;
		this.roleName = roleName;
	}

	// Déclaration des getters et setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ERole getRoleName() {
		return roleName;
	}

	public void setRoleName(ERole roleName) {
		this.roleName = roleName;
	}
	@JsonIgnoreProperties({"roles"})
	public List<Utilisateur> getUtilisateurs() {
		return utilisateurs;
	}

	public void setUtilisateurs(List<Utilisateur> utilisateurs) {
		this.utilisateurs = utilisateurs;
	}

	// Redéfinition de la méthode toString
	@Override
	public String toString() {
		return "Role [id=" + id + ", roleName=" + roleName + "]";
	}

}


//
//@Entity
//@Table(name = "roles")
//public class Role implements Serializable {
//	// Déclaration des attributs/	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name = "id_role", length = 36)
//	private int id;
//private String roleName;
//
//	@ManyToMany
//	@JoinTable(name = "Role_Utilisateur", joinColumns = @JoinColumn(name = "role_id"), inverseJoinColumns = @JoinColumn(name = "utilisateur_id"))
//	private List<Utilisateur> utilisateurs;
//
//	// Déclaration des constructeurs
//	// Constructeur vide
//	public Role() {
//		super();
//}
//
//	// Constructeur sans id
//	public Role(String roleName) {
//		super();
//		this.roleName = roleName;
//	}
//
//	// constructeur global
//public Role(int id, String roleName) {
//		super();
//		this.id = id;
//		this.roleName = roleName;
//	}
//	// Déclaration des getters et setters
//	public int getId() {
//		return id;
//	}
//public void setId(int id) {
//		this.id = id;
//	}
//
//	public String getRoleName() {
//		return roleName;
//	}
//	public void setRoleName(String roleName) {
//	this.roleName = roleName;	}
//
//	public List<Utilisateur> getUtilisateurs() {
//	return utilisateurs;
//}
//
//	public void setUtilisateurs(List<Utilisateur> utilisateurs) {
//		this.utilisateurs = utilisateurs;
//	}
//
//	// Redéfinition de la méthode toString
//	@Override
//	public String toString() {
//		return "Role [id=" + id + ", roleName=" + roleName + "]";
//	}
//
//}

