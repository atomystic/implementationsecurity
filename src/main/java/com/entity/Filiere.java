package com.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "filieres")
public class Filiere implements Serializable {

	// Attributs

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_filiere", length = 36)
	private int id;
	private String nom;
	private int nbreJours;
	private int dureeSoutenance;

	@OneToMany(mappedBy = "filiere", cascade = CascadeType.REMOVE)
	@JsonIgnore
	private List<Etudiant> etudiants;

	@OneToMany(mappedBy = "filiere", cascade = CascadeType.REMOVE)
	@JsonIgnore
	private List<Session> sessions;

	@OneToMany(mappedBy = "filiere", cascade = CascadeType.REMOVE)
	@JsonIgnore
	private List<Sujet> sujets;

//	
	// Constructeurs
	public Filiere() {
		super();
	}

	public Filiere(String nom, int nbreJours, int dureeSoutenance) {
		super();
		this.nom = nom;
		this.nbreJours = nbreJours;
		this.dureeSoutenance = dureeSoutenance;
	}

	public Filiere(int id, String nom, int nbreJours, int dureeSoutenance) {
		super();
		this.id = id;
		this.nom = nom;
		this.nbreJours = nbreJours;
		this.dureeSoutenance = dureeSoutenance;
	}

	// Getters et setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNbreJours() {
		return nbreJours;
	}

	public void setNbreJours(int nbreJours) {
		this.nbreJours = nbreJours;
	}

	public int getDureeSoutenance() {
		return dureeSoutenance;
	}

	public void setDureeSoutenance(int dureeSoutenance) {
		this.dureeSoutenance = dureeSoutenance;
	}

	public List<Etudiant> getEtudiants() {
		return etudiants;
	}

	public void setEtudiants(List<Etudiant> etudiants) {
		this.etudiants = etudiants;
	}

	public List<Session> getSessions() {
		return sessions;
	}

	public void setSessions(List<Session> sessions) {
		this.sessions = sessions;
	}

	public List<Sujet> getSujets() {
		return sujets;
	}

	public void setSujets(List<Sujet> sujets) {
		this.sujets = sujets;
	}

	// toString
	@Override
	public String toString() {
		return "Filiere [id=" + id + ", nom=" + nom + ", nbreJours=" + nbreJours + ", dureeSoutenance="
				+ dureeSoutenance + "]";
	}

}
